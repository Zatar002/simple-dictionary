﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace hwapi
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            definitionListView.IsVisible = false;
        }

        async void Handle_GetDefinition(object sender, EventArgs e)
        {
            bool connection = CrossConnectivity.Current.IsConnected;

            if (connection)
            {
                if (userEntry.Text != null)
                {
                    string userString = userEntry.Text;
                
                    userString = userString.ToLower();

                    HttpClient client = new HttpClient();
                    string dictionaryEndpoint = "https://owlbot.info/api/v2/dictionary/" + userString + "?format=json";
                    Uri dictionaryUri = new Uri(dictionaryEndpoint);
                    HttpResponseMessage response = await client.GetAsync(dictionaryEndpoint);

                    if (response.IsSuccessStatusCode)
                    {

                        string jsonContent = await response.Content.ReadAsStringAsync();
                        List<Define> myDefinitions = JsonConvert.DeserializeObject<List<Define>>(jsonContent);
                        if (myDefinitions.Count != 0)
                        {
                            ObservableCollection<Define> myDefinitionCollection = new ObservableCollection<Define>();

                            myDefinitions.ForEach(myDefinitionCollection.Add);

                            userWord.Text = userEntry.Text.ToUpper();
                            definitionListView.ItemsSource = myDefinitionCollection;
                            definitionListView.IsVisible = true;
                        }
                        else
                        {
                            userWord.Text = "MISSING OR NOT A WORD";    //strings of letters that are not words, but still return success
                            definitionListView.IsVisible = false;
                        }
                    }
                    else
                    {
                        userWord.Text = "MISSING OR NOT A WORD";    //for empty strings
                        definitionListView.IsVisible = false;
                    }
                }
                else
                {
                    userWord.Text = "MISSING OR NOT A WORD";    //for dealing with strings containing illegal characters (including spaces that does not return a success response)
                }
            }
            else
            {
                await DisplayAlert("No Internet", "No Internet Connection Detected", "OK");
                definitionListView.IsVisible = false;
            }
        }
    }
}
