﻿using System;
using System.Collections.Generic;
using System.Text;

namespace hwapi
{

    class Define
    {
        public string Type { get; set; }
        public string Definition { get; set; }
        public string Example { get; set; }
    }
}
